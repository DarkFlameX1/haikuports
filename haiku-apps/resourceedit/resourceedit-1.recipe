DESCRIPTION="
This is an editor for resource files (.rsrc), binary files that get compiled \
into an application and can then be accessed by that application.
"
SUMMARY="Resource editor"
HOMEPAGE="https://github.com/HaikuArchives/ResourceEdit"
LICENSE="MIT"
COPYRIGHT="2012-2013 Tri-Edge AI"
SRC_URI="git+https://github.com/HaikuArchives/ResourceEdit.git#1873c33591e2d2ccb3dccb692c175e9f10a0e450"
REVISION="1"

ARCHITECTURES="!x86 ?x86_64"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
	# x86_gcc2 is fine as primary target architecture as long as we're building
	# for a different secondary architecture.
	ARCHITECTURES="$ARCHITECTURES x86_gcc2"
else
	ARCHITECTURES="$ARCHITECTURES !x86_gcc2"
fi
SECONDARY_ARCHITECTURES="!x86"

PROVIDES="
	resourceedit$secondaryArchSuffix = $portVersion
	app:ResourceEdit = $portVersion
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	"
BUILD_PREREQUIRES="
	makefile_engine
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:make
	cmd:mkdepend
	"
	
BUILD()
{
	make
}

INSTALL()
{
	# supports "x86" architecture only
	mkdir $appsDir
	cp objects.x86-gcc4-release/ResourceEdit $appsDir
	addAppDeskbarSymlink $appsDir/ResourceEdit
}
