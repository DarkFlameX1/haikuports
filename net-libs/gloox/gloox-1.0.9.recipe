SUMMARY="Full-featured Jabber/XMPP client library"
DESCRIPTION="
Rock-solid, full-featured Jabber/XMPP client library, written in clean ANSI C++.
"
HOMEPAGE="http://camaya.net/gloox/"
LICENSE="GNU GPL v2"
COPYRIGHT="2002-2012 Jakob Schröter"
SRC_URI="http://camaya.net/download/gloox-1.0.9.tar.bz2"
CHECKSUM_SHA256="143dd50e1edc4eb1d304fa28bdd6ab9e53b60c37c1726dd0e34c06c51f9a453e"
REVISION="1"
ARCHITECTURES="x86 ?x86_64"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
	# x86_gcc2 is fine as primary target architecture as long as we're building
	# for a different secondary architecture.
	ARCHITECTURES="$ARCHITECTURES x86_gcc2"
else
	ARCHITECTURES="$ARCHITECTURES !x86_gcc2"
fi
SECONDARY_ARCHITECTURES="!x86_gcc2 x86"
PATCHES="gloox-1.0.9.patch"

PROVIDES="
	gloox$secondaryArchSuffix = $portVersion
	lib:libgloox$secondaryArchSuffix = $portVersion
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libssl$secondaryArchSuffix
	lib:libcrypto$secondaryArchSuffix
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libssl$secondaryArchSuffix
	devel:libcrypto$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	cmd:gcc$secondaryArchSuffix
	cmd:make
	cmd:libtoolize
	cmd:aclocal
	cmd:autoconf
	cmd:automake
	"
BUILD()
{
	libtoolize --force --copy --install
	aclocal
	autoconf
	automake
	runConfigure ./configure
	make
}

INSTALL()
{
	make install
	
	# prepare development lib links
	prepareInstalledDevelLib libgloox
	
	# devel package
	packageEntries devel $developDir
}

TEST()
{
	make check
}


# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	gloox${secondaryArchSuffix}_devel = $portVersion
	devel:libgloox$secondaryArchSuffix = $portVersion
	"
REQUIRES_devel="
	gloox$secondaryArchSuffix == $portVersion base
	"
